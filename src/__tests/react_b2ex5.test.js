import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import App from "../App.js";

configure({ adapter: new Adapter() });

const children = shallow(<App />).props().children;

describe("b2ex05: Refactor ECommerce clone", () => {
  it("App.js must render 2 components containing 'List' in their name", () => {
    let count = 0;
    const loop = (children) => {
      children.forEach((child) => {
        if (`${child.type}`.split(" ")[1]?.split("(")[0].includes("List")) {
          count++;
        } else if (
          child.props.children.length > 1 &&
          typeof child.props.children === "object"
        ) {
          loop(child.props.children);
        }
      });
    };
    loop(children);

    if (count !== 2) return false;
  });
  it("App.js must pass products through props the products array using key 'products' to the components", () => {
    let count = 0;
    const loop = (children) => {
      children.forEach((child) => {
        if (child.props.products) {
          count++;
        } else if (
          child.props.children.length > 1 &&
          typeof child.props.children === "object"
        ) {
          loop(child.props.children);
        }
      });
    };
    loop(children);

    if (count !== 2) return false;
  });
  it("List.js must render 3 Item components", () => {
    let count1 = 0,
      count2 = 0;
    const loop = (children) => {
      children.forEach((child, i) => {
        if (child.props.products?.length === 3) {
          count1++;
        } else if (child.props.products?.length === 2) {
          count2++;
        } else if (
          child.props.children.length > 1 &&
          typeof child.props.children === "object"
        ) {
          loop(child.props.children);
        }
      });
    };
    loop(children);

    if (count1 !== 1 || count2 !== 1) return false;
  });
});
