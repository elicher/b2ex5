import React from 'react'
import '../App.css'


const DisplayBest = (props) => {
	let { ele } = props;
	let { index } = props;
	let { check } = props;
	return (<article key={index} className="card">
      <img key={index} src={ele.image} className="image"/>
      <h2 key={index}>{ele.product}</h2>
      <p key={index}>{ele.category}</p>
      <div key={index} className="buyNow">
      <p key={index}>{ele.price}</p>
      <button key={index}>BUY</button>
      <span class="sale">{check}</span>
      </div>
      </article>)
	}

export default DisplayBest;
