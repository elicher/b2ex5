import React from 'react'
import '../App.css'


const Header = () => { 
	return(
<header className="App-header">
    <h1 className="h1header">My shop</h1>
    <ul id="header-ul">
    <li className="list-item">Item 1</li>
    <li className="list-item">Item 2</li>
    <li className="list-item">Item3</li>
    </ul>
   </header>)
}
   export default Header;
